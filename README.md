# Prueba técnica para Java Backend en Schibsted #

## Comando para construir la aplicación ##
```
#!java
mvn clean compile assembly:single
```
El jar generado estará en el directorio target con el nombre test-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar

## Comando para ejecutar los tests ##
```
#!java
mvn test
```

## Comando para ejecutar la apliación ##
```
#!java
java - jar test-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar
```

La aplicación arrancará en el puerto 8000.
El endpoint de la api rest para user es
/api/user

Los usuarios que he creado son:

UserName  | Password  |  Roles
------------- | ------------- | -------------
Fernando  | 123456 | PAGE_1 + ADMIN
Toni  | 123456 | PAGE_2
Sergio  | 123456 | PAGE_2