package com.schibsted.java.web;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import com.schibsted.java.TestApp;
import com.schibsted.java.domain.User;
import com.schibsted.java.services.SessionService;
import com.schibsted.java.services.UserService;
import com.schibsted.utils.Utils;

public class LoginContext extends CommonPageContext {

	@Override
	protected void manageRequest() throws IOException {
    	
    	if(httpExchange.getRequestMethod().equals("POST")){
    		String body = Utils.getStringFromInputStream(httpExchange.getRequestBody());
    		String userName = "";
    		String password = "";
    		String[] parameters = body.split("&");
    		for(int i = 0; i < parameters.length; i++){
    			String[] parameterPair = parameters[i].split("=");
    			String key = parameterPair[0];
    			String value = parameterPair[1];
    			if(key.equals("userName")){
    				userName = value;
    			}else if(key.equals("password")){
    				password = value;
    			}
    		}
    		User user = UserService.getInstance().findOne(userName);
    		if(user.getPassword().equals(password)){
    			SessionService.getInstance().deleteSession(httpExchange);
    			session = SessionService.getInstance().getSession(httpExchange, true);
    			session.put(USER_SESSION_KEY, user);
    			if(session.containsKey(REDIRECT_URI_KEY)){
    				redirectTo((String)session.get(REDIRECT_URI_KEY));
    			}else{
        			if(user.getRoles().get(0).equals("PAGE_3")){
        				redirectTo("/page3");
        			}else if(user.getRoles().get(0).equals("PAGE_2")){
        				redirectTo("/page2");
        			}else{
        				redirectTo("/page1");
        			}
    			}
    		}else{
    			//Redirect to login page
        		redirectTo("/login");
    		}
    	}else{
    		InputStream inputStream = TestApp.class.getResourceAsStream("/login.html");
            pageResult = Utils.getStringFromInputStream(inputStream);
            httpExchange.sendResponseHeaders(200, pageResult.length());
    	}
	}

	@Override
	protected String getRequiredRole() {
		return "";
	}
}
