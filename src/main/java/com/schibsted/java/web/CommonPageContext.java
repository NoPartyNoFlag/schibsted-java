package com.schibsted.java.web;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Map;

import com.schibsted.java.domain.User;
import com.schibsted.java.services.SessionService;
import com.schibsted.java.services.UserService;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public abstract class CommonPageContext implements HttpHandler{

	public static final String USER_SESSION_KEY = "USER_SESSION";
	public static final String LAST_USER_ACCESS_KEY = "LAST_USER_ACCESS";
	public static final String REDIRECT_URI_KEY = "REDIRECT_URI";
	public static final String AUTHORIZATION_HEADER_KEY = "Authorization";
	protected String pageResult;
	protected HttpExchange httpExchange;
	protected Map<String, Object> session;
	
	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
		this.httpExchange = httpExchange;
    	this.session = SessionService.getInstance().getSession(httpExchange);
    	pageResult = "";
    	
    	checkAuthorizationHeader();

    	if("".equals(getRequiredRole())){
    		manageRequest();
    	}else{
        	if(session.containsKey(USER_SESSION_KEY)){
            	User user = (User) session.get(USER_SESSION_KEY);
            	if(user.hasRole(getRequiredRole())){
            		manageRequest();
            	}else{
            		sendNotAuthorized();
            	}
        	}else{
        		if(httpExchange.getRequestURI().getPath().contains("/api")){
        			sendNotAuthorized();
        		}else{
        			session.put(REDIRECT_URI_KEY, httpExchange.getRequestURI().getPath());
            		redirectTo("/login");
        		}
        	}
    	}
        
    	closeResponse();
	}
	
	protected abstract void manageRequest() throws IOException;
	protected abstract String getRequiredRole();
	
	protected void redirectTo(String newLocation) throws IOException{
		//Redirect to login page
		httpExchange.getResponseHeaders().add("Location", newLocation);
		httpExchange.sendResponseHeaders(302, pageResult.length());
	};
	
	protected void sendNotAuthorized() throws IOException{
		httpExchange.sendResponseHeaders(403, pageResult.length());
	};
	
	private void checkAuthorizationHeader(){
		String authorizationHeader = httpExchange.getRequestHeaders().getFirst(AUTHORIZATION_HEADER_KEY);
		if(authorizationHeader != null && !"".equals(authorizationHeader)){
			int indexOfCode = authorizationHeader.indexOf("Basic ");
			if(indexOfCode >= 0){
				String value64 = authorizationHeader.substring(indexOfCode + "Basic ".length());
		        byte[] base64decodedBytes;
				try {
					base64decodedBytes = Base64.getDecoder().decode(value64);
					String value = new String(base64decodedBytes, "utf-8");
					String[] credentials = value.split(":");
					String userName = credentials[0];
					String password = credentials[1];
					User user = UserService.getInstance().findOne(userName);
					if(user != null && user.getPassword().equals(password)){
						session.put(USER_SESSION_KEY, user);
					}
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	
	protected void closeResponse() throws IOException{
        OutputStream os = httpExchange.getResponseBody();
        os.write(pageResult.getBytes());
        os.close();
	}
}
