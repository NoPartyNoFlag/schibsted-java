package com.schibsted.java.web;

import java.io.IOException;
import java.io.InputStream;

import com.schibsted.java.TestApp;
import com.schibsted.java.domain.User;
import com.schibsted.utils.Utils;

public class Page2Context extends CommonPageContext {

	@Override
	protected void manageRequest() throws IOException {
		InputStream inputStream = TestApp.class.getResourceAsStream("/index.html");
        pageResult = Utils.getStringFromInputStream(inputStream);
        User user = (User)session.get(USER_SESSION_KEY) ;
        pageResult = pageResult.replaceAll("#\\{name\\}", user.getUsername());
        pageResult = pageResult.replaceAll("#\\{pageName\\}", "Page 2");
        
        httpExchange.sendResponseHeaders(200, pageResult.length());
	}

	@Override
	protected String getRequiredRole() {
		return "PAGE_2";
	}

}
