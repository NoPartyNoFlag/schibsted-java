package com.schibsted.java.web;

import java.io.IOException;
import java.io.InputStream;

import com.schibsted.java.TestApp;
import com.schibsted.java.domain.User;
import com.schibsted.utils.Utils;

public class Page1Context extends CommonPageContext{

	@Override
	protected void manageRequest() throws IOException {
		User user = (User)session.get(USER_SESSION_KEY);
		InputStream inputStream = TestApp.class.getResourceAsStream("/index.html");
        pageResult = Utils.getStringFromInputStream(inputStream);
        pageResult = pageResult.replaceAll("#\\{name\\}", user.getUsername());
        pageResult = pageResult.replaceAll("#\\{pageName\\}", "Page 1");
        
        httpExchange.sendResponseHeaders(200, pageResult.length());
	}

	@Override
	protected String getRequiredRole() {
		return "PAGE_1";
	}
}
