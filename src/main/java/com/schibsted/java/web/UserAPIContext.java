package com.schibsted.java.web;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.schibsted.java.domain.User;
import com.schibsted.java.services.UserService;

public class UserAPIContext extends CommonPageContext {
    
    ObjectMapper mapper = new ObjectMapper();

	@Override
	protected void manageRequest() throws IOException {
    	
    	if(httpExchange.getRequestMethod().equals("PUT")){
            User user = mapper.readValue(httpExchange.getRequestBody(), User.class);
            UserService.getInstance().update(user);
            httpExchange.sendResponseHeaders(200, pageResult.length());
    	}else if(httpExchange.getRequestMethod().equals("POST")){
    		User user = mapper.readValue(httpExchange.getRequestBody(), User.class);
    		UserService.getInstance().insert(user);
    		pageResult = mapper.writeValueAsString(user);
            httpExchange.sendResponseHeaders(200, pageResult.length());
    	}else if(httpExchange.getRequestMethod().equals("DELETE")){
    		int lastPathVariableIndex = httpExchange.getRequestURI().getPath().lastIndexOf("/");
    		String userName = httpExchange.getRequestURI().getPath().substring(lastPathVariableIndex + 1);
    		if(UserService.getInstance().delete(userName)){
    			httpExchange.sendResponseHeaders(200, pageResult.length());
    		}else{
    			httpExchange.sendResponseHeaders(409, pageResult.length());
    		}
            
    	}else if(httpExchange.getRequestMethod().equals("GET")){
    	    if(httpExchange.getRequestURI().getPath().equals("/api/user/me")){
                User user = (User)session.get(USER_SESSION_KEY);
                pageResult = mapper.writeValueAsString(user);
    	    }else if(httpExchange.getRequestURI().getPath().matches("/api/user/[A-Za-z]+")){
    	    	int indexOf = httpExchange.getRequestURI().getPath().lastIndexOf("/");
    	    	String userName = httpExchange.getRequestURI().getPath().substring(indexOf + 1);
    	        User user = UserService.getInstance().findOne(userName);
    	        pageResult = mapper.writeValueAsString(user);
    	    }else if(httpExchange.getRequestURI().getPath().equals("/api/user")){
    	        List<User> users = UserService.getInstance().findAll();
    	        pageResult = mapper.writeValueAsString(users);
    	    }

            httpExchange.sendResponseHeaders(200, pageResult.length());
    	}
	}

	@Override
	protected String getRequiredRole() {
		return "ADMIN";
	}
}
