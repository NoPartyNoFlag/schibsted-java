package com.schibsted.java.web;

import java.io.IOException;

import com.schibsted.java.services.SessionService;

public class LogoutContext extends CommonPageContext {

	@Override
	protected void manageRequest() throws IOException {
		SessionService.getInstance().deleteSession(httpExchange);	
		redirectTo("/login");
	}

	@Override
	protected String getRequiredRole() {
		return "";
	}



}
