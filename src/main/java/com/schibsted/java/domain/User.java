package com.schibsted.java.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	private String username;
	private String password;
	private List<String> roles;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@JsonIgnore
	public String getPassword() {
		return password;
	}
	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}
	
	public List<String> getRoles() {
        return roles;
    }
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
    
    public boolean hasRole(String role){
        boolean hasRole = false;
        for(String userRole : roles){
            if(userRole.equals(role)){
                hasRole = true;
            }
        }
        return hasRole;
    }
    
    public boolean equals(Object o){
	    boolean equals = false;
	    if(this == o){
	        equals = true;
	    }
	    if(o != null && o instanceof User){
	        User user = (User)o;
	        if(this.getUsername().equals(user.getUsername())){
	            equals = true;
	        }
	    }
	    
	    return equals;
	}
	
	public int hashCode(){
	    return username.hashCode();
	}
}
