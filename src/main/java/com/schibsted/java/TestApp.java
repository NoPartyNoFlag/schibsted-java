package com.schibsted.java;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import com.schibsted.java.web.LoginContext;
import com.schibsted.java.web.LogoutContext;
import com.schibsted.java.web.Page1Context;
import com.schibsted.java.web.Page2Context;
import com.schibsted.java.web.Page3Context;
import com.schibsted.java.web.UserAPIContext;
import com.sun.net.httpserver.HttpServer;

public class TestApp {

	private static HttpServer server;

	public static void main(String[] args) throws Exception {
		server = HttpServer.create(new InetSocketAddress(8000), 0);
		server.createContext("/page1", new Page1Context());
		server.createContext("/page2", new Page2Context());
		server.createContext("/page3", new Page3Context());
		server.createContext("/login", new LoginContext());
		server.createContext("/logout", new LogoutContext());
		server.createContext("/api/user", new UserAPIContext());
		server.setExecutor(Executors.newCachedThreadPool());
		server.start();
		System.out.println("Started at 8000!!");
	}

	public static void serverStop(){
		server.stop(0);
	}
}
