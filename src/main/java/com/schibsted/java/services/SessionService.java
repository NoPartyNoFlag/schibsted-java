package com.schibsted.java.services;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.schibsted.java.web.CommonPageContext;
import com.schibsted.utils.Utils;
import com.sun.net.httpserver.HttpExchange;

public class SessionService {
	
    static private SessionService singletonInstance;
	private static final String SESSION_KEY = "JSESSIONID=";
	private static final String RESPONSE_COOKIE_HEADER_KEY = "Set-Cookie";
	public static final String REQUEST_COOKIE_HEADER_KEY = "Cookie";

	volatile private Map<String, Map<String, Object>> session = new ConcurrentHashMap<String, Map<String, Object>>();;
	
	private SessionService(){
	    
	}
	
    public static SessionService getInstance(){
        if(singletonInstance == null){
            singletonInstance = new SessionService();
        }
        return singletonInstance;
    }
	
	public Map<String, Object> getSession(HttpExchange httpExchange){
		return getSession(httpExchange, false);
	} 
	
	public Map<String, Object> getSession(HttpExchange httpExchange, boolean forceNewSession){
		Map<String, Object> sessionMap = null;
		String sessionKey = getSessionId(httpExchange);
		if(!"".equals(sessionKey) && !forceNewSession){
			sessionMap = session.get(sessionKey);
			if(sessionMap != null && sessionMap.containsKey(CommonPageContext.LAST_USER_ACCESS_KEY)){
				Date lastAccess = (Date) sessionMap.get(CommonPageContext.LAST_USER_ACCESS_KEY);
				long millisecondsDiff = new Date().getTime() - lastAccess.getTime();
				//Session timeout!!
				if((millisecondsDiff / 1000 / 60) > 5){
					session.remove(sessionKey);
					sessionMap = null;
				}
			}
			
			if(sessionMap == null){
				sessionMap = new ConcurrentHashMap<String, Object>();
				session.put(sessionKey, sessionMap);
			}
    	}else{
    		String sessionId = Utils.nextSessionId();
    		sessionMap = new ConcurrentHashMap<String, Object>();
    		session.put(sessionId, sessionMap);
    		String sessionCookie = SESSION_KEY + sessionId + "; Path=/; HttpOnly";
    		httpExchange.getResponseHeaders().add(RESPONSE_COOKIE_HEADER_KEY, sessionCookie);
    	}
		sessionMap.put(CommonPageContext.LAST_USER_ACCESS_KEY, new Date());
		return sessionMap;
	}
	
	public void deleteSession(HttpExchange httpExchange){
		String sessionKey = getSessionId(httpExchange);
		session.remove(sessionKey);
	}
	
	private String getSessionId(HttpExchange httpExchange){
		String sessionKey = "";
		String cookieHeader = httpExchange.getRequestHeaders().getFirst(REQUEST_COOKIE_HEADER_KEY);
		if(cookieHeader != null && !"".equals(cookieHeader)){
    		String[] cookies = cookieHeader.split(";");
        	for(int i = 0; i < cookies.length; i++){
        		String cookie = cookies[i];
        		int indexOfSessionKey = cookie.indexOf(SESSION_KEY);
        		if(indexOfSessionKey >= 0){
        			sessionKey = cookie.substring(indexOfSessionKey + SESSION_KEY.length());
        		}
        	}
		}
		return sessionKey;
	}
}
