package com.schibsted.java.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.schibsted.java.domain.User;

public class UserService {
    static private UserService singletonInstance;
    volatile private List<User> users = Collections.synchronizedList(new ArrayList<User>());


    private UserService(){
        User user = new User();
        user.setUsername("Fernando");
        user.setPassword("123456");
        List<String> roles = new ArrayList<String>();
        roles.add("PAGE_1");
        roles.add("ADMIN");
        user.setRoles(roles);
        users.add(user);

        user = new User();
        user.setUsername("Toni");
        user.setPassword("123456");
        roles = new ArrayList<String>();
        roles.add("PAGE_2");
        user.setRoles(roles);
        users.add(user);


        user = new User();
        user.setUsername("Sergio");
        user.setPassword("123456");
        roles = new ArrayList<String>();
        roles.add("PAGE_3");
        user.setRoles(roles);
        users.add(user);
    };
    
    public static UserService getInstance(){
        if(singletonInstance == null){
            singletonInstance = new UserService();
        }
        return singletonInstance;
    }

    public List<User> findAll(){
        return users;
    }
    
    public User findOne(String userName){
    	User userFilter = new User();
    	userFilter.setUsername(userName);
    	User userFound = null;
        int indexOfUser = users.indexOf(userFilter);
        if(indexOfUser >= 0){
        	userFound = users.get(indexOfUser);
        }
        return userFound;
    }

    public void update(User user){
        int indexOfUser = users.indexOf(user);
        if(indexOfUser >= 0){
            users.set(indexOfUser, user);
        }
    }
    
    public User insert(User user){
        users.add(user);
        return user;
    }
    
    public boolean delete(String userName){
    	User user = new User();
    	user.setUsername(userName);
    	int indexOf = users.indexOf(user);
    	if(indexOf >= 0){
    		users.remove(indexOf);
    	}
    	return indexOf >= 0;
    }
}
