package com.schibsted.java.test.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.schibsted.java.TestApp;

public class LoginContextTest {
	
	@BeforeClass
	public static void setup() throws Exception{
		TestApp.main(null);
	}
	
	@AfterClass
	public static void shutDown(){
		TestApp.serverStop();
	}

	@Test
	public void testGetLoginPage() throws IOException{
		String url = "http://localhost:8000/login";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		int responseCode = con.getResponseCode();
		//print result
		assertEquals(responseCode, 200);
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		assertTrue(response.toString().contains("<title>Login</title>"));
	}
	
}
