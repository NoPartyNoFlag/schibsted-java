package com.schibsted.java.test.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.schibsted.java.TestApp;

public class Page1ContextTest {
	
	@BeforeClass
	public static void setup() throws Exception{
		TestApp.main(null);
	}
	
	@AfterClass
	public static void shutDown(){
		TestApp.serverStop();
	}

	@Test
	public void unauthenticatedUserTest() throws IOException{
		String url = "http://localhost:8000/page1";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		int responseCode = con.getResponseCode();
		//print result
		assertEquals(responseCode, 200);
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		//Si no está autenticado. Se vuelve al login
		assertTrue(response.toString().contains("<title>Login</title>"));
	}
	
	@Test
	public void authenticateUserTest() throws IOException{
		String url = "http://localhost:8000/page1";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setInstanceFollowRedirects(false);
		String cookie = con.getHeaderField("Set-Cookie");
		
		url = "http://localhost:8000/login";
		obj = new URL(url);
		con = (HttpURLConnection) obj.openConnection();
		con.setInstanceFollowRedirects(false);
		con.setRequestProperty("Cookie", cookie);
		con.setRequestMethod("POST");
		String urlParameters = "userName=Fernando&password=123456";
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		assertEquals(con.getResponseCode(), 302);
		assertEquals(con.getHeaderField("Location"), "/page1");
	}
	
	@Test
	public void unauthorizedRoleAccessTest() throws IOException{
		String url = "http://localhost:8000/page1";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setInstanceFollowRedirects(false);
		String cookie = con.getHeaderField("Set-Cookie");
		
		url = "http://localhost:8000/login";
		obj = new URL(url);
		con = (HttpURLConnection) obj.openConnection();
		con.setInstanceFollowRedirects(false);
		con.setRequestProperty("Cookie", cookie);
		con.setRequestMethod("POST");
		String urlParameters = "userName=Toni&password=123456";
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.getResponseCode();
		cookie = con.getHeaderField("Set-Cookie");
		
		url = "http://localhost:8000/page1";
		obj = new URL(url);
		con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setInstanceFollowRedirects(false);
		con.setRequestProperty("Cookie", cookie);

		assertEquals(con.getResponseCode(), 403);
	}
}
