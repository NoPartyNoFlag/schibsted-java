package com.schibsted.java.test.web;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.schibsted.java.TestApp;
import com.schibsted.java.domain.User;

public class UserApiTest {
	
	ObjectMapper mapper = new ObjectMapper();
	
	@BeforeClass
	public static void setup() throws Exception{
		TestApp.main(null);
	}
	
	@AfterClass
	public static void shutDown(){
		TestApp.serverStop();
	}

	@Test
	public void getUsersTest() throws IOException{
		String url = "http://localhost:8000/api/user";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
        String base64encodedString = Base64.getEncoder().encodeToString("Fernando:123456".getBytes("utf-8"));
		con.setRequestProperty("Authorization", "Basic " + base64encodedString);
		assertEquals(con.getResponseCode(), 200);
	}
	
	@Test
	public void getUsersUnauthorizedTest() throws IOException{
		String url = "http://localhost:8000/api/user";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
        String base64encodedString = Base64.getEncoder().encodeToString("Toni:123456".getBytes("utf-8"));
		con.setRequestProperty("Authorization", "Basic " + base64encodedString);

		assertEquals(con.getResponseCode(), 403);
	}
	
	@Test
	public void createNewUser() throws IOException{
		User user = new User();
		user.setUsername("Pepe");
		user.setPassword("123456");
		List<String> roles = new ArrayList<String>();
		roles.add("PAGE_1");
		user.setRoles(roles);
		
		String url = "http://localhost:8000/api/user";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
        String base64encodedString = Base64.getEncoder().encodeToString("Fernando:123456".getBytes("utf-8"));
		con.setRequestProperty("Authorization", "Basic " + base64encodedString);
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(mapper.writeValueAsString(user));
		wr.flush();
		wr.close();

		assertEquals(con.getResponseCode(), 200);
	}
	
	@Test
	public void updateUser() throws IOException{
		User user = new User();
		user.setUsername("Sergio");
		user.setPassword("654321");
		List<String> roles = new ArrayList<String>();
		roles.add("PAGE_1");
		user.setRoles(roles);
		
		String url = "http://localhost:8000/api/user";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("PUT");
        String base64encodedString = Base64.getEncoder().encodeToString("Fernando:123456".getBytes("utf-8"));
		con.setRequestProperty("Authorization", "Basic " + base64encodedString);
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(mapper.writeValueAsString(user));
		wr.flush();
		wr.close();

		assertEquals(con.getResponseCode(), 200);
	}
	
	@Test
	public void deleteUser() throws IOException{
		String url = "http://localhost:8000/api/user/Toni";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("DELETE");
        String base64encodedString = Base64.getEncoder().encodeToString("Fernando:123456".getBytes("utf-8"));
		con.setRequestProperty("Authorization", "Basic " + base64encodedString);

		assertEquals(con.getResponseCode(), 200);
	}
	
	@Test
	public void findOneUser() throws IOException{
		
		String url = "http://localhost:8000/api/user/Fernando";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
        String base64encodedString = Base64.getEncoder().encodeToString("Fernando:123456".getBytes("utf-8"));
		con.setRequestProperty("Authorization", "Basic " + base64encodedString);
		
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		User user = mapper.readValue(response.toString(), User.class);
		User assertUser = new User();
		assertUser.setUsername("Fernando");

		assertEquals(user, assertUser);
	}

}
